/*
  XBee.cpp - Library for transmitting
  information with an XBee set on API mode.
   
  Will be useful in the process of creating 
  sensor nodes.

  Created by Cristóbal Liendo, July 15, 2016.
  Released into the public domain.
*/

#include "Arduino.h"
#include "XBee.h"
#include "SoftwareSerial.h"

XBee::XBee(int rx, int tx, byte dtr_pin, byte on_pin){
  module = new SoftwareSerial(rx, tx);

  _DTR_PIN = dtr_pin;
  _ON_PIN = on_pin;

  pinMode(_DTR_PIN, OUTPUT);
  digitalWrite(_DTR_PIN, HIGH);

  pinMode(_ON_PIN, INPUT);
}

void XBee::begin(int baudrate){
  module->begin(baudrate);
}

byte XBee::checksum(int length){
  byte total = 0xFF - (length & 0xFF);
  
  return total;
}

byte XBee::packet_length(byte hsize, String payload, byte p_byte){
  if(p_byte == false){
    byte plen = hsize + payload.length();

    if(plen > 255){
      return 1;
    } else {
      return plen;
    }
  }
  else{
    return hsize + 4;
  }
}

void XBee::transmit(String payload, boolean isByte, unsigned long byte_message){
  byte frame_length = packet_length(14, payload, isByte);

  int header = 526;

  module->write(0x7E);
  module->write((byte) 0);
  module->write(frame_length);

  module->write(0x10);
  module->write(0x01);

  for(byte i = 0; i < 8; i++){
    module->write((byte) 0);
  }

  module->write(0xFF);
  module->write(0xFE);

  module->write((byte) 0);
  module->write((byte) 0);

  if (isByte == false){
    for (int i = 0; i < payload.length(); i++){
      header += (char) payload[i];
      module->write((char) payload[i]);
    }
  } else {
    byte current_byte;

    for (byte i = 1; i <= 4; i++){
      current_byte = obtainByteInPosition(byte_message, i);
      header += current_byte;

      module->write(current_byte);
    }
  }

  module->write(checksum(header));
}

byte XBee::obtainByteInPosition(unsigned long payload, byte position){
  byte returned = 0;
  byte mask = 1;

  switch(position){
    case 2:
      payload >>= 16;
      break;

    case 3:
      payload >>= 8;
      break;

    case 4:
      payload >>= 0;
      break;

    default:
      payload >>= 24;
      break;
  }

  for(mask = 1; mask > 0; mask <<= 1){
    returned += (payload & mask);
  }

  return returned;
}

unsigned long XBee::insertByteInPosition(byte information, byte position){
  unsigned long returned = 0;
  
  returned |= information;

  switch (position){
    case 2: 
      returned <<= 16;
      break;

    case 3:
      returned <<= 8;
      break;

    case 4:
      break;

    default:
      returned <<= 24;
      break;
  }

  return returned;
}

unsigned long XBee::status_read(){
  unsigned long answer = 0;

  byte answer_length;
  byte ftype; 

  byte temp; 

  while (module->available() != 0){
    if (module->read() == 0x7E){
      module->read();

      answer_length = module->read();
      ftype = module->read();

      if (ftype == 0x90){
        if ((answer_length - 12) != 4){
          for(byte i = 0; i < answer_length - 1; i++){
            module->read();
          }

          module->read();

          return 0;
        }

        for (byte i = 0; i < 8; i++){
          module->read();
        }

        for (byte i = 0; i < 2; i++){
          module->read();
        }

        module->read();

        for (byte i = 1; i <= 4; i++){
          temp = module->read();
          answer += insertByteInPosition(temp, i);
        }

        module->read();

        return answer;
      }
      else {
        for (byte i = 0; i < answer_length-1; i++){
          module->read();
        }

        module->read();

        return 0;
      }
    }
  }

  return 0;
}

boolean XBee::incomming_data(){
  return module->available();
}

void XBee::sleep(){
  pinMode(_DTR_PIN, OUTPUT);
  digitalWrite(_DTR_PIN, HIGH);

}

void XBee::wake(){
  pinMode(_DTR_PIN, OUTPUT);
  digitalWrite(_DTR_PIN, LOW);
}

boolean XBee::is_awake(){
  if (digitalRead(_ON_PIN) == HIGH){
    return true;
  } 
  else {
    return false;
  }
}