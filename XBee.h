/*
  XBee.h - Library for enabling communication
  between XBees set on API mode.

  Created by Cristóbal Liendo, July 15, 2016.
  Released into the public domain.
*/
  
#ifndef XBee_h
#define XBee_h

#include "Arduino.h"
#include "SoftwareSerial.h"

class XBee
{
	public:
		XBee(int rx, int tx, byte dtr_pin, byte on_pin);		

		byte obtainByteInPosition(unsigned long payload, byte position);

		unsigned long status_read();
		unsigned long insertByteInPosition(byte information, byte position);

		void wake();
		void sleep();
		void begin(int baudrate);
		void transmit(String payload, boolean isByte=false, unsigned long byte_message=0);
		
		boolean is_awake();
		boolean incomming_data();

	private:
		SoftwareSerial* module;

		byte _DTR_PIN;
		byte _ON_PIN;

		byte checksum(int length);
		byte packet_length(byte hsize, String payload, byte p_byte);
};

#endif